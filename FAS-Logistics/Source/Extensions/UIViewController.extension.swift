//
//  UIViewController.extension.swift
//  AdventCalendar
//
//  Created by Matej Trbara on 15/09/15.
//  Copyright (c) 2015 Recordbay. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func localize() {
        self.view.localize()
        
        if let navTitle = self.navigationItem.title {
            self.navigationItem.title = Localize.string(navTitle)
        }
        
        
        if let nav = self as? UINavigationController {
            for vc in nav.viewControllers {
                vc.localize()
            }
        }
    }
}