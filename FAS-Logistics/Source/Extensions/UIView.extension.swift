//
//  UIView.extension.swift
//  AdventCalendar
//
//  Created by Matej Trbara on 18/08/15.
//  Copyright (c) 2015 Recordbay. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    func localize() {
    
        if let view = self as? UILabel {
            view.text = Localize.string(view.text!)
        } else if let view = self as? UIButton {
            if let title = view.titleForState(UIControlState.Normal) {
                view.setTitle(Localize.string(title), forState: UIControlState.Normal)
            }
            
        } else if let view = self as? UITextView {
            view.text = Localize.string(view.text!)
        } else if let view = self as? UITextField {
            view.text = Localize.string(view.text!)
            if let placeholder = view.placeholder {
                view.placeholder = Localize.string(placeholder)
            }
            
        } else if let view = self as? UISearchBar {
            view.text = Localize.string(view.text!)
            if let placeholder = view.placeholder {
                view.placeholder = Localize.string(placeholder)
            }
        }
        
        for view in self.subviews {
            view.localize()
        }
        
    }
}