//
//  Localize.swift
//  AdventCalendar
//
//  Created by Matej Trbara on 15/09/15.
//  Copyright (c) 2015 Recordbay. All rights reserved.
//

import Foundation

class Localize: NSObject {

    static func string(string: String) -> String {
        let result: String = NSLocalizedString(string, comment: "")
        if result.isEmpty {
            return string
        }
        
        return result
    }
}

