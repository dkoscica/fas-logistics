//
//  GenericSearchViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class BaseTableViewController : UIViewController {
    
    var numberOfSections = 1;
    var cellHeight : CGFloat = 60;
    var barTintColor = UIColor.whiteColor();
    
    var items = [AnyObject]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    @IBAction func onButtonClick(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }

}

// MARK: - UITableViewDataSource
extension BaseTableViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cellHeight
    }
    
}

// MARK: - UITableViewDelegate
extension BaseTableViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {}
    
}