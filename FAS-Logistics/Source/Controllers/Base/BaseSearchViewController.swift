//
//  GenericSearchViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class BaseSearchViewController : UIViewController {
    
    var numberOfSections = 1;
    var cellHeight : CGFloat = 60;
    var barTintColor = UIColor.whiteColor();
    
    var items = [AnyObject]()
    var filteredItems = [AnyObject]()
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        searchController.searchBar.tintColor = UIColor.whiteColor()
        searchController.searchBar.barTintColor = barTintColor
        searchController.hidesNavigationBarDuringPresentation = false
        tableView.tableHeaderView = searchController.searchBar
        
    }
    
    @IBAction func onButtonClick(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        self.filteredItems = self.items.filter({( item : AnyObject) -> Bool in
            return false
//            return item.
//            (offer["title"] as! String).lowercaseString.containsString(searchText.lowercaseString)
//                ||
//                (offer["cardname"] as! String).lowercaseString.containsString(searchText.lowercaseString)
//                ||
//                (offer["sitename"] as! String).lowercaseString.containsString(searchText.lowercaseString)
        })
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension BaseSearchViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return filteredItems.count
        }
        return items.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cellHeight
    }
    
}

// MARK: - UITableViewDelegate
extension BaseSearchViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {}
    
}

// MARK: - UISearchBarDelegate
extension BaseSearchViewController: UISearchBarDelegate {
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
}

// MARK: - UISearchResultsUpdating
extension BaseSearchViewController: UISearchResultsUpdating {
    
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!, scope: "")
    }
    
}
