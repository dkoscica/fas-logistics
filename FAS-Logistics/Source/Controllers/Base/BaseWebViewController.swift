//
//  BaseWebViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class BaseWebViewController: BaseViewController {

    var navigationTitle: String!
    var webViewURL: NSURL!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = navigationTitle
        
        if webViewURL != nil {
            loadWebView()
        }
    }
    
    func loadWebView() {
        let urlRequest = NSURLRequest(URL: webViewURL!);
        webView.loadRequest(urlRequest);
    }
}
