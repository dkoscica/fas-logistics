//
//  BaseViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {

    private var menuItems = [MenuItem]()
    var menuType = MenuType.Login
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localize()
        
        initNavigationBar()
        addSlideMenuButton()
        initMenuView()
    }
    
    private func initNavigationBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.barTintColor = AppConstants.Color.Red
            navigationBar.tintColor = .whiteColor()
            navigationBar.translucent = false
            navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName : UIColor.whiteColor()
            ]
        }
    }
    
    private func initMenuView() {
        switch menuType {
            case .Login: initLoginMenu()
            case .Client: initClientMenu()
            case .Transporter: initTransporterMenu()
        }
    }
    
    private func initLoginMenu() {
        self.menuItems = [
            MenuItem(title: Localize.string("settings"), icon: "icon_sidemenu_settings_normal.png", url: ""),
            MenuItem(title: Localize.string("terms"), icon: "icon_sidemenu_terms_normal.png", url: ""),
            MenuItem(title: Localize.string("about"), icon: "icon_sidemenu_about_normal.png", url: "")
        ]
    }
    
    private func initClientMenu() {
        self.menuItems = [
            MenuItem(title: Localize.string("settingsClient"), icon: "icon_sidemenu_settings_normal.png", url: ""),
            MenuItem(title: Localize.string("terms"), icon: "icon_sidemenu_terms_normal.png", url: ""),
            MenuItem(title: Localize.string("about"), icon: "icon_sidemenu_about_normal.png", url: "")
        ]
    }
    
    private func initTransporterMenu() {
        self.menuItems = [
            MenuItem(title: Localize.string("settingsTransporter"), icon: "icon_sidemenu_settings_normal.png", url: ""),
            MenuItem(title: Localize.string("terms"), icon: "icon_sidemenu_terms_normal.png", url: ""),
            MenuItem(title: Localize.string("about"), icon: "icon_sidemenu_about_normal.png", url: "")
        ]
    }
    
    func pushController(viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func pushControllerNamed(navigationTitle: String, controllerName: String) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil);
        if let controller = storyBoard.instantiateViewControllerWithIdentifier(controllerName) as? BaseViewController {
            controller.title = navigationTitle
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func popToPreviousViewController() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func pushBaseWebViewController(navigationTitle : String, url:NSURL){
        let storyBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        if let baseWebViewController = storyBoard.instantiateViewControllerWithIdentifier("BaseWebViewController") as? BaseWebViewController {
            baseWebViewController.navigationTitle = navigationTitle
            baseWebViewController.webViewURL = url
            navigationController?.pushViewController(baseWebViewController, animated: true)
        }
    }
    
    //MARK: - MenuViewController
    func slideMenuItemSelectedAtIndex(index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            pushControllerNamed("Settings", controllerName: "SetttingsViewController")
    
        case 1:
//            let storyBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//            if let profileRegistrationViewController = storyBoard.instantiateViewControllerWithIdentifier("ProfileRegistrationViewController") as? ProfileRegistrationViewController {
//                navigationController?.pushViewController(profileRegistrationViewController, animated: true)
//            }
            pushBaseWebViewController("Terms & Conditions", url: NSURL(string: "http://www.fas-logistika.hr/en/onama.php")!)
            
        case 2:
            pushBaseWebViewController("About", url: NSURL(string: "http://www.fas-logistika.hr/en/onama.php")!)
            
        default:
            print("Error")
        }
    }
    
    private func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButtonType.Custom)
        btnShowMenu.setImage(UIImage(named: "ic_drawer.png"), forState: UIControlState.Normal)
        btnShowMenu.frame = CGRectMake(0, 0, 30, 30)
        btnShowMenu.addTarget(self, action: "onSlideMenuButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    func onSlideMenuButtonPressed(sender : UIButton){
        if (sender.tag == 10) {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.mainScreen().bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clearColor()
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.enabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
        menuVC.menuButton = sender
        menuVC.delegate = self
        menuVC.menuItems = menuItems
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRectMake(0 - UIScreen.mainScreen().bounds.size.width, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height);
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            menuVC.view.frame=CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height);
            sender.enabled = true
            }, completion:nil)
    }
    
    
}
