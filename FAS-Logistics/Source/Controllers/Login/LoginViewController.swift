//
//  LoginViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, LoginView {

    @IBOutlet weak var usernameTextField: WhiteTextField!
    
    @IBOutlet weak var passwordTextField: WhiteTextField!
    
    var presenter: LoginPresenterProtocol!
    
    override func viewDidLoad() {
        
        self.menuType = .Login

        super.viewDidLoad()
        
        self.title = "Login"
        
        presenter = LoginPresenter(view: self)
        
        usernameTextField.addIcon("icon_username_big.png")
        passwordTextField.addIcon("icon_password_big.png")
    }
    
    @IBAction func signIn(sender: UIButton) {
        let username = usernameTextField?.text as String!
        let password = passwordTextField?.text as String!

        presenter.validateUsernameAndPassword(username, password: password)
    
    }
    
    func signInFailed() {
        print("FAIL")
    }
    
    func signInSuccessful() {
        print("SUCCES")
    }
    
}
