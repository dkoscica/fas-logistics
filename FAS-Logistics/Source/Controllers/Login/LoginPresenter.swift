//
//  LoginPresenter.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 27/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

protocol LoginPresenterProtocol {

    func validateUsernameAndPassword(username: String, password: String)

}

class LoginPresenter: LoginPresenterProtocol {
    
    let view: LoginView!
    
    required init(view: LoginView) {
        self.view = view
    }
    
    func validateUsernameAndPassword(username: String, password: String) {
        if(username == "T" && password == "T") {
            view.signInSuccessful()
            return
        }
        view.signInFailed()
    }

}
