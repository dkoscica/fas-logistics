//
//  MenuViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

public protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(index : Int32)
}

public class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet var menuTableView : UITableView!
    @IBOutlet var closeMenuTransparentOverlay : UIButton!
    
    var menuItems = [MenuItem]()
    
    var menuButton : UIButton!
    
    var delegate : SlideMenuDelegate?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localize()

        
        menuTableView.tableFooterView = UIView()
        menuTableView.separatorStyle = .None
    }
    
    @IBAction func onCloseMenuClick(button:UIButton!){
        menuButton.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.closeMenuTransparentOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.frame = CGRectMake(-UIScreen.mainScreen().bounds.size.width, 0, UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clearColor()
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("menuCell")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        imgIcon.contentMode = .ScaleAspectFit
        
        imgIcon.image = UIImage(named: self.menuItems[indexPath.row].icon)
        
        lblTitle.text = self.menuItems[indexPath.row].title;
        
        return cell
    }
    
    public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let btn = UIButton(type: UIButtonType.Custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60;
    }
    
    public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }

}
