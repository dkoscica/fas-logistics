//
//  MenuItem.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class MenuItem: NSObject {

    var title : String!
    var icon : String!
    var url : String!
    
    convenience init(title : String, icon : String, url : String) {
        self.init()
        self.title = title
        self.icon = icon
        self.url = url
    }
    
}
