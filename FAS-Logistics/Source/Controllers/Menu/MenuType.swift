//
//  MenuType.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 27/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

enum MenuType {
    
    case Login
    case Client
    case Transporter

}
