//
//  SettingsPresenter.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

protocol SettingsPresenterProtocol {
    func initLanguageSpinner()
}


class SettingsPresenter: SettingsPresenterProtocol {
    
    func initLanguageSpinner() {
        
    }

}
