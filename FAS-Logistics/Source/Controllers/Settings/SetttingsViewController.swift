//
//  SetttingsViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class SetttingsViewController: BaseViewController {

    var presenter: SettingsPresenterProtocol!
    var pickerDataSource = ["White", "Red", "Green", "Blue"]
    
    @IBOutlet weak var languagePickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SettingsPresenter()
        
        self.languagePickerView.dataSource = self;
        self.languagePickerView.delegate = self;
        
    }
}

extension SetttingsViewController: UIPickerViewDelegate {

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        pickerLabel.textColor = UIColor.blackColor()
        pickerLabel.text = self.pickerDataSource[row]
        pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 14)
        pickerLabel.textAlignment = NSTextAlignment.Center
        return pickerLabel
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerDataSource[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("dsada")
    }
    
}

extension SetttingsViewController: UIPickerViewDataSource {

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
}
