//
//  CountrySearchPresenter.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

protocol CountrySearchPresenterProtocol {
    
    func loadCountries() -> [Country]
    func persistSelectedCountry(selectedCountry : Country)
    
}


class CountrySearchPresenter: CountrySearchPresenterProtocol {

    var selectedCountry : Country!
    
    func loadCountries() -> [Country] {
    
        var countries  = [Country]()
        
        countries.append(Country(countryId: 1, countryName: "Croatia"))
        countries.append(Country(countryId: 2, countryName: "Germany"))

        return countries
    }
    
    func persistSelectedCountry(selectedCountry : Country) {
        self.selectedCountry = selectedCountry
    }
    
}
