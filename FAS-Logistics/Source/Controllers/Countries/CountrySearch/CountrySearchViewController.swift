//
//  CountrySearchViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class CountrySearchViewController : BaseSearchViewController {

    var presenter : CountrySearchPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = CountrySearchPresenter()
        
        self.cellHeight = 40
        self.barTintColor = AppConstants.Color.Gray
        self.items = presenter.loadCountries()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CountryCell", forIndexPath: indexPath) as! CountryCell
        cell.layoutMargins = UIEdgeInsetsZero
        
        var country = self.items[indexPath.row] as! Country
        
        if searchController.active && searchController.searchBar.text != "" {
            country = filteredItems[indexPath.row] as! Country
        } else {
            country = self.items[indexPath.row] as! Country
        }
        
        cell.countryName.text = country.countryName

        return cell
    }
    
    override func filterContentForSearchText(searchText: String, scope: String = "All") {
        self.filteredItems = self.items.filter({( item : AnyObject) -> Bool in
            let country = item as! Country
            return country.countryName.lowercaseString.containsString(searchText.lowercaseString)
        })
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.presenter.persistSelectedCountry(self.items[indexPath.row] as! Country)
    }

}
