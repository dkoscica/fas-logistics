//
//  CountriesTableViewController.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class CountriesTableViewController: BaseTableViewController {
    
    var presenter : CountriesPresenterProcotol!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CountriesPresenter()
        
        tableView.registerNib(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        self.items = presenter.loadCountries()
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CountryCell", forIndexPath: indexPath) as! CountryCell
        cell.layoutMargins = UIEdgeInsetsZero
        
        let country = self.items[indexPath.row] as! Country
    
        cell.countryName.text = country.countryName
        
        return cell
    }
    
}
