//
//  CountriesPresenter.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

protocol CountriesPresenterProcotol {

    func loadCountries() -> [Country]
    
}


class CountriesPresenter: NSObject, CountriesPresenterProcotol {

    
    func loadCountries() -> [Country] {
        
        var countries  = [Country]()
        
        countries.append(Country(countryId: 1, countryName: "Croatia"))
        countries.append(Country(countryId: 2, countryName: "Germany"))
        
        return countries
    }
    
}
