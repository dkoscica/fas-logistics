//
//  DataDeals.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

//
// The Class DataDeals is a simple Java Bean that is used to hold Background path ,
// title , subtitle , pices and the bought product details.
//

class DataDeals: NSObject {
    
    var backgroundMain: Int!
    var headTitle: String!
    var subTitle: String!
    var strikedPrice: Int!
    var newPrice: Int!
    var boughtDeals: Int!
    
    override init() {}
    
    convenience init(backgroundMain: Int, headTitle: String, subTitle: String, strikedPrice: Int, newPrice: Int, boughtDeals: Int) {
        self.init()
        
        self.backgroundMain = backgroundMain
        self.headTitle = headTitle
        self.subTitle = subTitle
        self.strikedPrice = strikedPrice
        self.newPrice = newPrice
        self.boughtDeals = boughtDeals
    }

}
