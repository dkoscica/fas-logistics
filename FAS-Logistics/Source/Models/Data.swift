//
//  Data.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

//
// The Data class hold the information of the image and the title
// text for the listview options .
//

class Data: NSObject {
    
    var title: String!
    var sel: String!

    var image: Int!
    var imageSelected: Int!

}
