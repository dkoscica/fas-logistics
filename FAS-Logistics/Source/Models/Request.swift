//
//  Request.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class Request: NSObject {

    var requestId: Int!
    var userId: Int!
    var loadCountryId: Int!
    var unloadCountryId: Int!

    var loadCountry: String!
    var unloadCountry: String!
    var loadZipCode: String!
    var unloadZipCode: String!
    var loadCity: String!
    var unloadCity: String!
    var loadDate: String!
    var unloadDate: String!
    var koleta: String!
    var measure: String!
    var weight: String!
    var goodsType: String!
    var express: String!
    var notice: String!
    var status: String!
    var dateTime: String!

}
