//
//  Country.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class Country {

    var countryId: Int!
    var countryName: String!
 
    init() {}

    convenience init(countryId: Int, countryName: String) {
        self.init()

        self.countryId = countryId
        self.countryName = countryName
    }
    
}
