//
//  AppModel.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class AppModel: NSObject, NSCoding {
    
    var user : User!
    var language : Language!
    
    
    required convenience init?(coder decoder: NSCoder) {
        self.init();
        self.user = decoder.decodeObjectForKey("user") as! User
        self.language = decoder.decodeObjectForKey("language") as! Language
        self.language = Language(rawValue: decoder.decodeObjectForKey("language") as! String)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.user, forKey: "user")
        aCoder.encodeObject(self.language.rawValue, forKey: "language")
    }
    
}
