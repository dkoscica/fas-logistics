//
//  RequestOffer.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class RequestOffer: NSObject {
    
    var requestOfferId: Int!
    var requestId: Int!
    var userId: Int!
    var loadDate: String!
    var unloadDate: String!
    var notice: String!
    var status: String!
    var dateTime: String!
    var priceIn: String!
    var priceOut: String!

}
