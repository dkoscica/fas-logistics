//
//  User.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

public enum UserType {

    case Client
    case Transporter

}


class User {
    
    var username: String!
    var password: String!
    var userId: Int!
    var userType : UserType!

}
