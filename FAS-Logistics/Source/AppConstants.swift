//
//  AppConstants.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 19/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

struct AppConstants {
    
    let API_BASE_URL = "http://fas.dev-flb.com";
    
    struct Color {
        
        static let Gray = UIColor(red: 161.0/255.0, green: 161.0/255.0, blue: 161.0/255.0, alpha: 1.0)
        static let DarkGray = UIColor(red: 158.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
        static let LightGray = UIColor(red: 220.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1.0)
        static let TextGray = UIColor(red: 87.0/255.0, green: 94.0/255.0, blue: 110.0/255.0, alpha: 1.0)

        static let Black = UIColor(red: 63.0/255.0, green: 61.0/255.0, blue: 61.0/255.0, alpha: 1.0)
        static let LightBlack = UIColor(red: 122.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 1.0)
        
        static let Red = UIColor(red: 255.0/255.0, green: 32.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        static let MenuColor = UIColor(red: 255.0/255.0, green: 201.0/255.0, blue: 195.0/255.0, alpha: 1.0)
        static let CouponColor = UIColor(red: 87.0/255.0, green: 94.0/255.0, blue: 110.0/255.0, alpha: 1.0)
        static let DealyDealColor = UIColor(red: 38.0/255.0, green: 51.0/255.0, blue: 66.0/255.0, alpha: 1.0)
        
    }

}
