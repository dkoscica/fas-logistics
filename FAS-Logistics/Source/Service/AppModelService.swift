//
//  AppModelService.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 26/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class AppModelService {
    
    static let sharedService = AppModelService()
    
    var appModel: AppModel!
    
    init() {
        self.appModel = self.loadAppModel()
    }
    
    func saveAppModel() {
        NSKeyedArchiver.archiveRootObject(self.appModel, toFile: self.path)
    }
    
    func loadAppModel() -> AppModel {
        guard let appModel = NSKeyedUnarchiver.unarchiveObjectWithFile(self.path) as? AppModel
            else {
                return AppModel()
        }
        return appModel
    }
    
    var path: String {
        get {
            let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
            return documentsFolderPath.stringByAppendingPathComponent("app_file")
        }
    }

}
