//
//  RedButton.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 20/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class RedButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.cornerRadius = 25.0;
        self.layer.borderColor = AppConstants.Color.Red.CGColor
        self.layer.borderWidth = 1.5
        
        self.backgroundColor = AppConstants.Color.Red
        
        self.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }

}
