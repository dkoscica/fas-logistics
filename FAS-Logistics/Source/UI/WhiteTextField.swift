//
//  WhiteTextField.swift
//  FAS-Logistics
//
//  Created by Dominik Koscica on 20/03/16.
//  Copyright © 2016 Fleksbit. All rights reserved.
//

import UIKit

class WhiteTextField: UITextField {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        layer.cornerRadius = 5.0
        layer.borderWidth = 1.5
        
        layer.borderColor = UIColor.whiteColor().CGColor
        backgroundColor = UIColor.whiteColor()
        textColor = AppConstants.Color.TextGray
        
        leftViewMode = UITextFieldViewMode.Always
    }
    
    func addIcon(imageName: String) {
        let iconImageView = UIImageView();
        
        let iconHeight = frame.height + 5
        let iconWidth = frame.height - 15
        
        iconImageView.frame = CGRect(x: 0, y: 0, width: iconHeight, height: iconWidth)
        addSubview(iconImageView)
        
        let iconImage = UIImage(named: imageName);
        iconImageView.image = iconImage;
        iconImageView.contentMode = .ScaleAspectFit
        leftView = iconImageView;
    }
    
}
